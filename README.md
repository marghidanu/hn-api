# HN::API

## Description

**Hacker News** web API client implemented as described [here](https://github.com/HackerNews/API).

## Development environment

	vagrant up
	vagrant ssh

	sudo su -
	cd /vagrant

	perl Build.PL
	./Build installdeps
	./Build
	./Build test
