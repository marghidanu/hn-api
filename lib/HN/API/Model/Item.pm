package HN::API::Model::Item {
	use Moose;
	# use Moose::Util::TypeConstraints;

	extends 'HN::API::Model';

	has 'id' => (
		is => 'ro',
		isa => 'Int',
		required => 1,
	);

	has 'deleted' => (
		is => 'ro',
		isa => 'Maybe[Bool]',
	);

	# TODO: Fix import so we can declare the types properly
	has 'type' => (
		is => 'ro',
		isa => 'Str',
		# isa => enum( [ qw( job story comment poll pollopt ) ] ),
		required => 1,
	);

	has 'by' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	has 'time' => (
		is => 'ro',
		isa => 'Int',
		required => 1,
	);

	has 'text' => (
		is => 'ro',
		isa => 'Maybe[Str]',
	);

	has 'dead' => (
		is => 'ro',
		isa => 'Maybe[Bool]',
	);

	has 'parent' => (
		is => 'ro',
		isa => 'Maybe[Int]',
	);

	has 'poll' => (
		is => 'ro',
		isa => 'Maybe[Int]',
	);

	has 'kids' => (
		is => 'ro',
		isa => 'ArrayRef[Int]',
		default => sub { [] }
	);

	has 'url' => (
		is => 'ro',
		isa => 'Maybe[Str]',
	);

	has 'score' => (
		is => 'ro',
		isa => 'Maybe[Int]',
	);

	has 'title' => (
		is => 'ro',
		isa => 'Maybe[Str]',
	);

	has 'parts' => (
		is => 'ro',
		isa => 'ArrayRef[Int]',
		default => sub { [] },
	);

	has 'descendants' => (
		is => 'ro',
		isa => 'Int',
		default => 0,
	);

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

HN::API::Model::Item

=head1 DESCRIPTION

Item model as described in the official documentation.

=head1 ATTRIBUTES

=head2 id

The item's unique id.

=head2 deleted

I<true> if the item is deleted.

=head2 type

The type of item. One of B<job>, B<story>, B<comment>, B<poll>, or B<pollopt>.

=head2 by

The username of the item's author.

=head2 time

Creation date of the item, in Unix Time.

=head2 text

The comment, story or poll text. HTML.

=head2 dead

true if the item is dead.

=head2 parent

The comment's parent: either another comment or the relevant story.

=head2 poll

The pollopt's associated poll.

=head2 kids

The ids of the item's comments, in ranked display order.

=head2 url

The URL of the story.

=head2 score

The story's score, or the votes for a pollopt.

=head2 title

The title of the story, poll or job.

=head2 parts

A list of related pollopts, in display order.

=head2 descendants

In the case of stories or polls, the total comment count.

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
