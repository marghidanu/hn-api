package HN::API::Model::User {
	use Moose;

	extends 'HN::API::Model';

	has 'id' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	has 'delay' => (
		is => 'ro',
		isa => 'Maybe[Int]',
	);

	has 'created' => (
		is => 'ro',
		isa => 'Int',
		required => 1,
	);

	has 'karma' => (
		is => 'ro',
		isa => 'Int',
		required => 1,
	);

	has 'about' => (
		is => 'ro',
		isa => 'Maybe[Str]',
	);

	has 'submitted' => (
		is => 'ro',
		isa => 'ArrayRef[Int]',
		default => sub { [] },
	);

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

HN::API::Model::User

=head1 DESCRIPTION

User model as described in the official documentation.

=head1 ATTRIBUTES

=head2 id

The user's unique username. Case-sensitive. Required.

=head2 delay

Delay in minutes between a comment's creation and its visibility to other users.

=head2 created

Creation date of the user, in Unix Time.

=head2 karma

The user's karma.

=head2 about

The user's optional self-description. HTML.

=head2 submitted

List of the user's stories, polls and comments.

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
