package HN::API::Model {
	use Moose;

	use MooseX::Storage;

	with Storage();

	around 'pack' => sub {
		my $orig = shift();
		my $self = shift();

		my $result = $self->$orig( @_ );
		delete( $result->{__CLASS__} );

		return $result;
	};

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

HN::API::Model

=head1 DESCRIPTION

Models base class with basic serialization.

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
