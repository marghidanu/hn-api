package HN::API::ModelFactory {
	use MooseX::AbstractFactory;

	implementation_class_via sub { sprintf( 'HN::API::Model::%s', shift() ) };
}

1;

__END__

=head1 NAME

HN::API::ModelFactory

=head1 DESCRIPTION

Factory of models.

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
