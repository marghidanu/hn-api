package HN::API {
	use Moose;

	our $VERSION = '0.0.0';

	use Carp;

	use Mojo::URL;
	use Mojo::UserAgent;

	use HN::API::ModelFactory;

	has 'base_url' => (
		is => 'ro',
		isa => 'Str',
		default => 'https://hacker-news.firebaseio.com/v0/'
	);

	has '_agent' => (
		is => 'ro',
		isa => 'Mojo::UserAgent',
		lazy => 1,
		default => sub {
			return Mojo::UserAgent->new();
		}
	);

	sub get_item {
		my ( $self, $id ) = @_;

		croak( 'No identifier provided!' )
			unless( defined( $id ) );

		my $data = $self->_get_data( sprintf( '/v0/item/%d.json', $id ) );
		croak( 'The server returned no value' )
			unless( defined( $data ) );

		return HN::API::ModelFactory->create( 'Item', $data );
	}

	sub get_user {
		my ( $self, $id ) = @_;

		croak( 'No identifier provided!' )
			unless( defined( $id ) );

		my $data = $self->_get_data( sprintf( '/v0/user/%s.json', $id ) );
		croak( "User '${id}' doesn't exist!" )
			unless( defined( $data ) );

		return HN::API::ModelFactory->create( 'User', $data );
	}

	sub max_item {
		my $self = shift();

		return $self->_get_data('/v0/maxitem.json' );
	}

	sub new_stories {
		my $self = shift();

		return $self->_get_data( '/v0/newstories.json' );
	}

	sub top_stories {
		my $self = shift();

		return $self->_get_data( '/v0/topstories.json' )
	}

	sub best_stories {
		my $self = shift();

		return $self->_get_data( '/v0/beststories.json' )
	}

	sub ask_stories {
		my $self = shift();

		return $self->_get_data( '/v0/askstories.json' )
	}

	sub show_stories {
		my $self = shift();

		return $self->_get_data( '/v0/showstories.json' );
	}

	sub job_stories {
		my $self = shift();

		return $self->_get_data( '/v0/jobstories.json' );
	}

	sub updates {
		my $self = shift();

		return $self->_get_data( '/v0/updates.json' );
	}

	sub _get_data {
		my ( $self, $path ) = @_;

		my $url = Mojo::URL->new( $self->base_url() );
		$url->path( $path );

		my $tx = $self->_agent()->get( $url );
		my $response = $tx->success();
		unless( $response ) {
			my $error = $tx->error()->{message};
			croak( "Error: ${error}" );
		}

		return $response->json();
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

HN::API

=head1 SYNOPSIS

	use HN::API

	my $hn = HN::API->new();

	# Getting a item
	my $item = $hn->get_item( 121003 );

	# Getting a user profile
	my $user = $hn->get_user( 'jl' );

	# Last item id
	my $last_item = $hn->max_item();

	# Getting some updates for HN
	my $updates = $hn->updates();

=head1 DESCRIPTION

HackerNews API client.

=head1 METHODS

=head2 ask_stories

=head2 best_stories

=head2 get_item

Stories, comments, jobs, Ask HNs and even polls are just items.
They're identified by their ids, which are unique integers.

=head2 get_user

Users are identified by case-sensitive ids.
Only users that have public activity (comments or story submissions) on the site are available through the API.

=head2 job_stories

=head2 max_item

The current largest item id. You can walk backward from here to discover all items.

=head2 new_stories

=head2 show_stories

=head2 top_stories

=head2 updates

The item and profile changes

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>

=head1 SEE ALSO

L<https://github.com/HackerNews/API>

=head1 LICENSE

The full text of the license can be found in the LICENSE.txt file included with this distribution.
