#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Exception;

require_ok( 'HN::API' );

my $instance = HN::API->new();
isa_ok( $instance, 'HN::API' );

# --- Items
{
	can_ok( $instance, qw( get_item max_item ) );

	my $last_item = $instance->max_item();
	ok( defined( $last_item ), 'Getting last id' );

	my $item = $instance->get_item( 121003 );
	isa_ok( $item, 'HN::API::Model' );
	isa_ok( $item, 'HN::API::Model::Item' );
	can_ok( $item, qw( pack ) );
	isa_ok( $item->pack(), 'HASH' );

	dies_ok { $instance->get_item() } 'No item parameter';
	dies_ok { $instance->get_item(-1) } 'Invalid item parameter';

	# use Data::Dumper;
	# warn( Dumper( $item ) );
}

# --- Users
{
	my $user = $instance->get_user( 'jl' );
	isa_ok( $user, 'HN::API::Model' );
	isa_ok( $user, 'HN::API::Model::User' );
	can_ok( $user, qw( pack ) );
	isa_ok( $user->pack(), 'HASH' );

	dies_ok { $instance->get_user() } 'No user parameter';
	dies_ok { $instance->get_user( 'marghi' ) } 'Invalid user parameter';

	# use Data::Dumper;
	# warn( Dumper( $user ) );
}

# --- Stories ( popularity )
{
	my $new = $instance->new_stories();
	ok( defined( $new ), 'new_stories has values' );
	isa_ok( $new, 'ARRAY' );

	my $top = $instance->top_stories();
	ok( defined( $top ), 'top_stories has values' );
	isa_ok( $top, 'ARRAY' );

	my $best = $instance->best_stories();
	ok( defined( $best ), 'best_stories has values' );
	isa_ok( $best, 'ARRAY' );
}

# --- Stories ( category )
{
	my $ask = $instance->ask_stories();
	ok( defined( $ask ), 'ask_stories has values' );
	isa_ok( $ask, 'ARRAY' );

	my $show = $instance->show_stories();
	ok( defined( $show ), 'show_stories has values' );
	isa_ok( $show, 'ARRAY' );

	my $job = $instance->job_stories();
	ok( defined( $job ), 'job_stories has values' );
	isa_ok( $job, 'ARRAY' );
}

# --- Updates
{
	my $updates = $instance->updates();
	ok( defined( $updates ), 'updates has values' );
	isa_ok( $updates, 'HASH' );

	isa_ok( $updates->{items}, 'ARRAY' );
	isa_ok( $updates->{profiles}, 'ARRAY' );
}

done_testing();
